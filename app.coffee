fs = require 'fs'
path = require 'path'
express = require 'express'
exphbs  = require 'express3-handlebars'
pkg = require './package.json'

app = express()
port = process.env.PORT || 8080
env = process.env.NODE_ENV || 'development'

console.log '\ninitializing ' + pkg.name + ' (' + env + ')'

# expose application properties to views
app.locals
  app:
    name:     pkg.name
    version:  pkg.version
    env:      env

# set Handlebars as the view engine
console.log '... initializing handlebars view engine'
hbs = exphbs.create
  defaultLayout:  'main'
  helpers:        require('./helpers')
app.engine 'handlebars', hbs.engine
app.set 'view engine', 'handlebars'
app.set 'views', __dirname + '/views'

# allow the application to sit behind a proxy
app.enable 'trust proxy'

# initialize middleware
console.log '... initializing middleware'
app.use express.favicon 'public/favicon.ico'
app.use express.logger()
app.use express.compress()
app.use express.static __dirname + '/public'
app.use express.cookieParser()
app.use express.bodyParser()
app.use express.methodOverride()
app.use app.router
app.use express.errorHandler dumpExceptions: true, showTrace: true

# initialize application routes
console.log '... initializing routes'
require('./routes').init app

# start server
app.listen port
console.log 'listening on port ' + port
