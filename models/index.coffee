mongoose = require 'mongoose'
config = require '../config'

class Models

  mongoose: mongoose

  constructor: () ->
    if mongoose.connection.readyState is 0
      mongoose.connect config.db.uri

  # instantiate models here
  Task: mongoose.model 'Task', require('./task')

module.exports = new Models()

