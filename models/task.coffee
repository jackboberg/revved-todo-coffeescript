moment = require 'moment'
mongoose = require 'mongoose'

TaskSchema = new mongoose.Schema
  description:
    type: String
    required: true
  creation_date:
    type: Date
    default: Date.now
  due_date:
    type: Date
    default: Date.now
  complete:
    type: Boolean

TaskSchema.virtual('past_due')
  .get () ->
    @.due_date and @.due_date < new Date()
TaskSchema.virtual('due_date_formatted')
  .get () ->
    return null unless @.due_date
    moment(this.due_date).format 'YYYY.MM.DD'

TaskSchema.set 'toJSON',
  virtual: true
  getters: true
  transform: (doc, ret, options) ->
    delete ret.__v
    delete ret._id
    ret

TaskSchema.statics.listSorted = (cb) ->
  @.model('Task')
    .find({})
    .sort('complete due_date')
    .exec(cb)

module.exports = TaskSchema
