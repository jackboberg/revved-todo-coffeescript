function loadTasks () {
  var taskTemplateSource =
    '<tr class="task {{#if past_due}}past-due{{/if}} {{#if complete}}complete{{/if}}">' +
    '<td>{{#unless complete}}<button class="complete-task-button" data-task-id="{{id}}">Complete</button>{{else}}&#10004;{{/unless}}</td>' +
    '<td><strong>{{description}}</td>' +
    '<td>{{due_date_formatted}}</td>' +
    '<td><button class="delete-task-button" data-task-id="{{id}}">Delete</button></td>' +
    '</tr>';
  var taskTemplate = Handlebars.compile(taskTemplateSource);

  $.ajax({
    url: '/api/tasks',
    method: 'get',
    contentType: 'application/json',
    cache: false,
    success: function(tasks, status, xhr) {
      var html = '';
      for (var i=0; i < tasks.length; i++) {
        html += taskTemplate(tasks[i]);
      };
      $('#task-list').html(html);
      initButtons();
    },
    error: function(xhr, status, error) {
      var response = JSON.parse(xhr.responseText);
      alert(response.error.message);
    }
  });
};

function addTask (data) {
  $.ajax({
    url: 'api/tasks',
    method: 'post',
    contentType: 'application/json',
    dataType: 'json',
    data: JSON.stringify(data),
    success: function (data, status, xhr) {
      loadTasks();
    },
    error: function(xhr,status,error){
      var response = JSON.parse(xhr.responseText);
      alert(response.error.message);
    }
  });
};

function completeTask (id) {
  $.ajax({
    url: '/api/tasks/' + id,
    method: 'put',
    contentType: 'application/json',
    dataType: 'json',
    data: '{"complete": true}',
    success: function(data,status,xhr){
      loadTasks();
    },
    error: function(xhr,status,error){
      var response = JSON.parse(xhr.responseText);
      alert(response.error.message);
    }
  });
};

function deleteTask (id) {
  $.ajax({
    url: '/api/tasks/' + id,
    method: 'delete',
    dataType: 'json',
    success: function(data,status,xhr){
      loadTasks();
    },
    error: function(xhr,status,error){
      var response = JSON.parse(xhr.responseText);
      alert(response.error.message);
    }
  });
};

function initButtons() {
  $('button.complete-task-button').on('click', function(event){
    var btn = $(event.target);
    completeTask(btn.data('task-id'));
  });
  $('button.delete-task-button').on('click', function(event){
    var btn = $(event.target);
    deleteTask(btn.data('task-id'));
  });
};

$(document).ready(function() {
  initButtons();
  $('#add-task-form').on('submit',function(event){
    var form = $(event.target);
    var data = {
      description: form.find('input[name=description]').val(),
      due_date: form.find('input[name=due_date]').val(),
    };
    addTask(data);
    event.preventDefault();
  });
});
