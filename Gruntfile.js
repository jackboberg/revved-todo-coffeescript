module.exports = function(grunt) {

  var path = require('path'),
      spawn = require('child_process').spawn;

  var app;
  var inspector;
  var debugPort = 5860;
  var inspectorPort = 6060;

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    clean: {
      node_modules: ['node_modules']
    },
    watch: {
      run: {
        files: ['**/*.js', '!public/**/*.js'],
        tasks: ['stop', 'run'],
        options: {
          nospawn: true
        }
      },
      debug: {
        files: ['**/*.js', '!public/**/*.js'],
        tasks: ['stop', 'debug'],
        options: {
          nospawn: true
        }
      }
    },
    jshint: {
      options: {
        curly: true,
        eqeqeq: true,
        immed: true,
        latedef: true,
        newcap: true,
        noarg: true,
        sub: true,
        undef: true,
        boss: true,
        eqnull: true,
        strict: false,
        globals: {
          define: true,
          require: true,
          exports: true,
          module: true
        }
      },
      client: {
        options: {
          jquery: true
        },
        files: [
          { src: ['public/js/**/*.js', '!public/js/lib/**/*.js'] }
        ]
      },
      server: {
        options: {
          node: true
        },
        files: [
          { src: ['**/*.js', '!node_modules/**/*.js', '!public/**/*.js'] }
        ]
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-jshint');

  grunt.registerTask('default', ['jshint']);

  grunt.registerTask('run', 'Runs app dir using node', function (nodeEnv, port) {
    var done = this.async();
    var env = process.env;
    env.NODE_ENV = nodeEnv || 'development';
    env.PORT = port || 8080;
    app = spawn('node', ['app.js'], {env: env, stdio: 'inherit'});
    grunt.task.run('watch:run');
    done();
  });

  grunt.registerTask('debug', 'Runs app dir using node', function (nodeEnv, port) {
    var done = this.async();
    var env = process.env;
    env.NODE_ENV = nodeEnv || 'development';
    env.PORT = port || 8080;
    app = spawn('node', ['--debug=' + debugPort, 'app.js'], {env: env, stdio: 'inherit'});
    grunt.task.run('run-inspector');
    grunt.task.run('watch:debug');
    done();
  });

  grunt.registerTask('stop', 'Stops the app if it is currently running', function () {
    if (app) {
      grunt.log.writeln('Stopping app ...');
      app.kill();
      app = null;
    }
    grunt.task.run('stop-inspector');
  });

  grunt.registerTask('run-inspector', 'Launches node-inspector and connects chrome debugger', function () {
    var done = this.async();
    inspector = spawn('node-inspector', ['--web-port=' + inspectorPort]);
    spawn('open', ['http://localhost:' + inspectorPort + '/debug?port=' + debugPort]);
    done();
  });

  grunt.registerTask('stop-inspector', 'Stops node-inspector', function () {
    if (inspector) {
      grunt.log.writeln('Stopping inspector ...');
      inspector.kill();
      inspector = null;
    }
  });

};
