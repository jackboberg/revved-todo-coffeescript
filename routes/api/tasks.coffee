models = require '../../models'

exports.list = (req, res, next) ->
  models.Task.listSorted (err, tasks) ->
    return next err if err
    res.json 200, tasks

exports.post = (req, res, next) ->
  task = req.body
  models.Task.create task, (err, task) ->
    return next err if err
    res.json 201, task

exports.get = (req, res, next) ->
  taskId = req.params.id
  models.Task.findById taskId, (err, task) ->
    return next err if err
    res.json 200, task

exports.put = (req, res, next) ->
  taskId = req.params.id
  taskBody = req.body
  models.Task.findByIdAndUpdate taskId, taskBody, (err, task) ->
    return next err if err
    res.json 200, task

exports.del = (req, res, next) ->
  taskId = req.params.id
  models.Task.findByIdAndRemove taskId, (err, task) ->
    return next err if err
    res.json 200, success: true
