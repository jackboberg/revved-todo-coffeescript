exports.init = (app) ->

  # register routes here
  home = require './home'
  app.all '/', home.index

  taskApi = require './api/tasks'
  app.get '/api/tasks', taskApi.list
  app.post '/api/tasks', taskApi.post
  app.get '/api/tasks/:id', taskApi.get
  app.put '/api/tasks/:id', taskApi.put
  app.delete '/api/tasks/:id', taskApi.del
