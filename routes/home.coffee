models = require '../models'

exports.index = (req, res, next) ->
  models.Task.listSorted (err, tasks) ->
    return next(err) if err
    res.render 'home',
      title: 'Revved Todo'
      tasks: tasks
